angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $http, $ionicSlideBoxDelegate) {
  $http.get('http://Be01.appmimimi.com/comment').then(function(data){
		console.log(data.data);
		$scope.chorosRelevantes = data.data;
		$ionicSlideBoxDelegate.update();
  }, function(error){
		console.log(error);
		$ionicLoading.hide();
  });
  
  $scope.doRefresh = function(){
	  $http.get('http://Be01.appmimimi.com/comment').then(function(data){
			console.log(data.data);
			$scope.chorosRelevantes = data.data
	  }, function(error){
			console.log(error);
			$ionicLoading.hide();
	  });
  }
  
  $scope.like = function(id){
	console.log('like');
	$http.post('http://Be01.appmimimi.com/comment/upvote/'+id).then(function(data){
		console.log(data);
		$scope.doRefresh();
	}, function(error){
		console.log(error);
	});
  }
  
  $scope.meh = function(id){
	console.log('meh');
	$http.post('http://Be01.appmimimi.com/comment/downvote/'+id).then(function(data){
		console.log(data);
		$scope.doRefresh();
	}, function(error){
		console.log(error);
	});
  }
})

.controller('MainCtrl', function($scope, $ionicActionSheet, $ionicSlideBoxDelegate, $http, $ionicLoading, $ionicScrollDelegate, $ionicModal, $timeout) {
	$scope.plus = 'ion-ios-plus-outline';
	$scope.closed = true;
	$scope.showOptions = 'hide-options';
	$scope.showChoro = 'hide-choro';
	$scope.showAnswer = 'hide-answer';
	$scope.slide = 'slide-2';
	$scope.choros = [];

	$ionicSlideBoxDelegate.enableSlide(false);

	//  $scope.openPostOptions = function(closed){
	if($scope.closed){
		$scope.plus = 'ion-ios-plus';
		$scope.showOptions = 'show-options';
		$scope.closed = false;
	}else{
		$scope.plus = 'ion-ios-plus-outline';
		$scope.showOptions = 'hide-options';
		$scope.closed = true;
	}
	//  }

	$scope.openTextInput = function(){
	$ionicSlideBoxDelegate.next();
	$scope.slide = 'slide-2';
	}

	$scope.interval = 5000;

	$scope.slideHasChanged = function(index) {
		$scope.slideIndex = index;
		if ( ($ionicSlideBoxDelegate.count() -1 ) == index ) {
			$timeout(function(){
				$ionicSlideBoxDelegate.slide(0);
			},$scope.interval);
		}
	};
  
	$scope.cry = function(cryData){
		$ionicScrollDelegate.scrollBottom(true);

		cryData.who = 'chupetator2000';

		$scope.choro = cryData.text;
	//	$scope.showOptions = 'hide-options';
		$scope.showChoro = 'show-choro';

		$scope.plus = 'ion-ios-plus-outline';
		$scope.slide = 'slide-2';
//		$scope.showLogo = 'hide';
		$scope.closed = true;

		$ionicLoading.show();
		$http.post('http://Be01.appmimimi.com/comment', cryData).then(function(data){
			console.log(data.data);
			
			$ionicLoading.hide();
			
//			$scope.choros.push({ choro: searchData.comment, answer: data.data.text });
			$scope.choros.choro = cryData.comment;
			$scope.choros.answer = data.data.text;
			
			cryData.text = '';
			$scope.showAnswer = 'show-answer';
			$scope.answersCarousel ='hide';
		}, function(error){
			console.log(error);
			$ionicLoading.hide();
		});
	}

	$ionicModal.fromTemplateUrl('templates/profile-modal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modalProfile = modal;
	});

	$scope.profileModalClose = function(){
		$scope.modalProfile.hide();
	}

	$scope.profileModal = function(){
		console.log('teste');
		$scope.modalProfile.show();
	}
});

